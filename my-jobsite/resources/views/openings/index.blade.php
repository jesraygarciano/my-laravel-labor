@extends('layouts.app')

@section('content')

<div class="container">
    <article>
        <h3>
            <h1>Search Jobs</h1>
        </h3>
    </article>
    <hr>
    <div class="container-fluid" id="opening">
        <div class="row">
            <div class="col-md-3 well" id="opening-search">
                <h4>Search criteria:</h4>
                {!!Form::open(['action' => 'OpeningsController@index', 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{-- {!!Form::label('opening_search', '')!!} --}}
                        {!!Form::text('opening_search', null, ['class' => 'form-control', 'placeholder' => 'Search'])!!}
                    </div>
                    <div class="form-group">
                        {!!Form::submit('Search', ['class' => 'btn btn-primary form-control'])!!}
                    </div>
                {!!Form::close()!!}
                <br/>
                {!!Form::open(['action' => 'OpeningsController@index', 'method' => 'POST']) !!}
                    <h4>Advanced search criteria:</h4>

                    <div class="form-group">
                        <label for="formGroupExampleInput2">Company:</label>
                        {!!Form::text('company_name', old('company_name'), ['class' => 'form-control','id' => 'formGroupExampleInput2',  'placeholder' => 'Company name'])!!}
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Location:</label>
                        <select class="form-control" id="exampleSelect1">
                            <option>Cebu</option>
                            <option>Manila</option>
                            <option>Davao</option>
                            <option>Bohol</option>
                            <option>Leyte</option>
                        </select>
                    </div>
                    <div class="form-check">
                        <label for="exampleSelect1">Languages:</label>

                        <div class="form-row">
                            <div class="col-xs-6">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    PHP
                                </label>
                            </div>

                            <div class="col-xs-6">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    Java
                                </label>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-xs-6">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    Ruby
                                </label>
                            </div>

                            <div class="col-xs-6">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input">
                                    Python
                                </label>
                            </div>
                        </div>
                    </div>

                    <br />
                    <br />
                    <br />
                    <div class="form-group">
                        <label for="exampleSelect1">Date posted:</label>
                        <select class="form-control" id="exampleSelect1">
                            <option>Past 24 hours</option>
                            <option>Past Week</option>
                            <option>Past Month</option>
                            <option>Anytime</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary form-control">Advanced search</button>
                {!!Form::close()!!}
            </div>

            <div class="col-md-7">
                @if (count($openings) > 0)
                    @foreach ($openings as $opening)
                        <div class="job-tile">
                            <div>
                                <span class="job-position featured">Featured</span>
                                <span class="job-position regular">Regular</span>
                                <span class="job-position intern">Intern</span>
                            </div>
                            <div class="job-title">
                                <a href="{{ url('openings', $opening->id) }}"> {{ $opening->title }} </a>
                                <img class="pull-right" src="{{ $opening->company->company_logo }}" alt="" border="0" height="100" width="130" style="max-width: 130px;">
                            </div>
                            <div class="company-name_opening_list"><a href="{{ url('companies', $opening->company->id) }}"> {{$opening->company->company_name}} </a></div>
                            <ul class="opening-feature-info-list">
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $opening->company->address1 }} </li>
                                <li><i class="fa fa-dollar" aria-hidden="true"></i> PHP 10,000 - 15,000 </li>
                                <li>
                                    <i class="fa fa-code" aria-hidden="true"></i>
                                    <div class="label label-warning">
                                        Java
                                    </div>
                                    <div class="label label-primary">
                                        Python
                                    </div>
                                    <div class="label label-info">
                                        <div class="hover-info">
                                            <div class="pointer"></div>
                                            <div class="content">
                                                <ul>
                                                    <li>JQuery</li>
                                                    <li>Angular</li>
                                                </ul>
                                            </div>
                                        </div>
                                        Javascript
                                    </div>
                                    <div class="label label-info">
                                        HTML
                                    </div>
                                </li>
                            </ul>
                            <hr class="opening-top-date-hr" style="margin-top: 7px; margin-bottom: 7px;">
                            <div class="footer">
                                <div class="pull-left">
                                    <div class="foggy-text"> {{ date(' M. j, Y ',strtotime($opening->created_at)) }} </div>
                                </div>
                                <div class="pull-right">
                                    <div class="foggy-text">
                                        @include('openings.opening_bookmark.bookmark_button', ['opening' => $opening])
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {!!$openings->appends(['company_name'=>$company_name])->render()!!}
                    {{-- {!! $openings->render() !!} --}}
                @endif
            </div>
            <div class="col-md-2 well">
                <h4>Advertisement</h4>
            </div>
        </div>
    </div>
</div>
@endsection
