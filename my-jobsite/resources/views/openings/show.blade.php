@extends('layouts.app')

@section('content')

<div class="container tall single-page">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
              <a href="#jobinfo" role="tab" data-toggle="tab">
                Job Information
              </a>
        </li>
        <li role="presentation">
              <a href="#companyinfo" role="tab" data-toggle="tab">
                Company Information
              </a>
        </li>
    </ul>
 
   <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="jobinfo">
          <div class="row">
            <div class="col-sm-12">
                <h2>Job Information</h2>
            </div>
          </div>
        <div id="show_opening"> {{-- START OF SHOW OPENING --}}
            <style type="text/css">
                .cover-image{
                    position: relative;
                    height: 300px;
                    background: #c8c8c8;
                    overflow: hidden;
                    border: 1px solid #cecece;
                }

                .cover-image img{
                    position: absolute;
                    top: 50%;
                    transform:translateY(-50%);
                    width: 100%;
                    left: 0px;
                }

                .cover-info .picture{
                    padding: 5px;
                    background: white;
                    border: 1px solid #cecece;
                    position: relative;
                }

                .cover-info .picture img{
                    width: 100%;
                }

                .cover-info{
                }

                .cover-info img{
                    border:none!important;
                }
            </style>
                    <hr>
                    <div class="row" id="openings-body" style="margin:0px;">
                        <div class="col-md-7">
                            <div class="job-description">
                                <h4>
                                    <i class="fa fa-file-text" aria-hidden="true"></i>
                                    Job description:
                                </h4>
                                <hr class="hr-desc" />
                                {{ $opening->details }}
                                {{ $opening->details }}
                                {{ $opening->details }}
                                {{ $opening->details }}
                                {{ $opening->details }}
                            </div>
                            <div class="job-qualify">
                                <h4>
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                    Job Qualifications:
                                </h4>
                                <hr />
                                <h5>{{ $opening->requirements }}</h5>
                                <h5>{{ $opening->requirements }}</h5>
                                <h5>{{ $opening->requirements }}</h5>
                            </div>

                        </div>

                        <div class="col-md-5">
                            <h4>
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                Company Profile:
                            </h4>
                            <hr>

                            <div class="row">
                                <div class="col-xs-6">
                                    Company size:
                                    <h5>
                                        <b>
                                            {{ $company->number_of_employee }}
                                        </b>
                                        Employees
                                    </h5>
                                    Telephone no.
                                    <h5>
                                        <b>
                                            {{ $company->tel }}
                                        </b>
                                    </h5>
                                    Country
                                    <h5>
                                        <b>
                                            {{ $company->country }}
                                        </b>
                                    </h5>
                                </div>
                                <div class="col-xs-6">
                                    Postal no.
                                    <h5>
                                        <b>
                                            {{ $company->postal }}
                                        </b>
                                    </h5>
                                    CEO:
                                    <h5>
                                        <b>
                                            {{ $company->ceo_name }}
                                        </b>
                                    </h5>
                                    Bill company name:
                                    <h5>
                                        <b>
                                            {{ $company->bill_company_name }}
                                        </b>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    {!! link_to(action('ApplicationController@create', [$opening->id]), 'Application Form', ['class' => 'btn btn-danger']) !!}
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="companyinfo">
<!--       <div class="row">
        <div class="col-lg-12">
            <h2>Company Information</h2>
        </div>
      </div> -->
                    <div class="row text-center">
                        <div class="col-md-12 cover-info">
                            <div class="cover-image">
                                <img src="{{ $opening->picture }}" alt="{{ $company->company_name}}" />
                            </div>
                            <div class="row cover-info" id="openings-title" style="margin:0px; margin-top:-100px;">
                                <div class="col-sm-2">
                                    <div class="picture">
                                        <div class="photo-wrapper">
                                            <img src="{{asset('img/bg-img.png')}}" class="bg-img">
                                            <img class="_image" src="{{ $company->company_logo }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row" style="text-align:left;">
                                        <div class="col-sm-6">
                                            <h1>
                                                <a href="{{ url('companies', $company['id']) }}">
                                                    {{ $company->company_name }}
                                                    <br>
                                                </a>
                                            </h1>
                                        </div>
                                        <div class="col-sm-6">
                                            <h5>
                                                <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                                                &nbsp; {{ $opening->city }}, {{ $opening->country }}
                                            </h5>
                                            <h5>
                                                <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
                                                &nbsp; {{ $opening->created_at }}
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


<!--         <div class="row">
            <div class="col-md-3">
                <img src="/storage/{{ $company->company_logo }}" alt="{{ $company->company_name}}" />
            </div>
            <div class="col-md-7">
                <h1>{{ $company->company_name }}</h1>
                    <span class="employee_number">
                        <i class="fa fa-users fa-2x" aria-hidden="true"></i>                    
                        {{ $company['number_of_employee'] }}
                        Employees                        
                    </span>
                    <br>
                    <span class="company-email">
                        <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                        {{ $company->email }}
                    </span>
            </div>
            <div class="col-md-2">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span>{{ $company->city }}</span>
            </div>
        </div> -->



        <div class="row" id="company-opening-body">
            <div class="col-md-7">

                <div class="row">
                    <div class="col-md-12">
                        <h3>
                            <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                Company overview:
                        </h3>
                        <p>
                            {{ $company->what }}
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        {{ $company->what }}
                        <h3>
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                                Why join us?
                        </h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-md-5">
                <h3>
                    <i class="fa fa-signal" aria-hidden="true"></i>
                    Company snapshots:
                </h3>
                <div>
                    Email address :
                     <h5>
                        <b>{{ $company->email }}</b>
                     </h5>
                     <br />
                 </div>
                <div>
                    Company Tel. no :
                     <h5>
                        <b>{{ $company->tel }}</b>
                     </h5>
                     <br />
                 </div>                 
                <div>
                    Company address :
                     <h5>
                        <b>{{ $company->address1 }}</b>
                     </h5>
                     <br />
                 </div>
                <div>
                    Company website :
                     <h5>
                        <b>{{ $company->url }}</b>
                     </h5>
                     <br />
                 </div>
                <div>
                    Company size :
                     <h5>
                        <b>{{ $company->url }}</b>
                     </h5>
                     <br />
                 </div>                 
                <div>in_charge : {{ $company->in_charge }}</div>
                <div>ceo_name : {{ $company->ceo_name }}</div>
                <div>number_of_employee : {{ $company->number_of_employee }}</div>                
            </div>
        </div>

        </div>
    </div>

</div>




@endsection
