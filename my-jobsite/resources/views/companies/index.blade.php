@extends('layouts.app')

@section('content')
<div class="container">
  <div id="company-list">
      <h2 class="text-center">Company lists</h2>
      <hr>
      <div id="company-search">
          <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  {!!Form::select('country', $country_array, null, ['class' => 'js-example-placeholder-multiple', 'multiple' => 'multiple', 'style' => 'width:100%'] )!!}
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <select class="js-example-placeholder-multiple multiple-city" name="states[]" multiple="multiple" placeholder="Select a State" style="width:100%;">
                    <option>Cebu</option>
                    <option>Davao</option>
                    <option>Siquijor</option>
                    <option>Negros</option>
                    <option>Bohol</option>
                    <option>Leyte</option>
                    <option>Samar</option>
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Company name search...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Search</button>
                    </span>
                  </div><!-- /input-group -->
              </div><!-- /.col-md-6 -->

          </div> <!-- End company search Row -->
      </div> <!-- End company-search -->

      <div id="company-single-lists">
          <div class="row">
              <div class="col-md-10">
                  <div class="row">
                         @if (count($companies) > 0)
                              @foreach ($companies as $company)
                                 <div id="first-comp-list" class="col-xs-12 col-sm-6 col-md-6">
                                      <div class="media well">
                                        <div class="media-body">
                                          <h3 class="mt-0 mb-1">
                                              <a href="{{ url('companies', $company['id']) }}">
                                                  {{ $company['company_name'] }}
                                                  <br>
                                                  {{-- {{ dd($company)}} --}}
                                              </a>
                                          </h3>
                                          <ul>
                                            <li>
                                                <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                                                {{ $company['city'] }},
                                                {{ $company['country'] }}
                                            </li>
                                            <li>
                                                <i class="fa fa-users fa-lg" aria-hidden="true"></i>
                                                {{ $company['number_of_employee'] }} <b>Employees</b>
                                            </li>
                                            <li>
                                                <i class="fa fa-laptop fa-lg" aria-hidden="true"></i>
                                                <a href="{{ $company['url'] }}">{{ $company['url'] }}</a>
                                            </li>
                                            <li>
                                                <i class="fa fa-language fa-lg" aria-hidden="true"></i>
                                                {{ $company['bill_country'] }}
                                            </li>
                                            <li>
                                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i>
                                                    <a href="{{ url('companies', $company['id']) }}">
                                                      {{ $company->openings->count() }} Current hiring
                                                    </a>
                                            </li>
                                            <li>
                                                <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
                                                  Latest job posted:
                                                  {{ $company['updated_at'] }}
                                            </li> 
                                          </ul>
                                        <div id="company-bookmark">
                                          <div class="d-flex align-self-end ml-3">
                                            @include('companies.follow_company.follow_button', ['company' => $company])                                            
                                          </div>
                                        </div>

                                        </div> <!-- media body -->
                                        <img class="d-flex ml-3" src="/storage/{{ $company->company_logo }}" alt="{{ $company->company_name}}" alt="{{ $company->company_name}}" height="150px;" width="150px;" />
                                      </div>
                                  </div>
                                      @endforeach
                                   {!! $companies->render() !!}
                                  @endif                                  
                         </div>
              </div>

              <div id="comp-advertisement" class="col-md-2">
                  <div class="well text-center">
                    <h4>Advertisement</h4>
                  </div>
              </div>
          </div>
      </div>

  <script type="text/javascript">
      $(document).ready(function() {
      $('.js-example-placeholder-multiple').select2(
      );
  });
  </script>
  </div>
</div>

@endsection

