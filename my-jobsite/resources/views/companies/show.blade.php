@extends('layouts.app')

@section('content')
<div class="container">
    <div class="single-page" class="container">
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
                <a data-toggle="tab" href="#compinfo">
                Company Information
                </a>
        </li>

        <li role="presentation">
            <a data-toggle="tab" href="#joblists">
                Opening Job lists
            </a>
        </li>

      </ul>

      <div class="tab-content">
        <div role="tabpanel" id="compinfo" class="tab-pane active"> {{-- START COMPANY INFO --}}
              <div class="row">
                <div class="col-sm-12">
                    <h2>Company Information</h2>
                </div>
              </div>
            <style type="text/css">
                .cover-image{
                    position: relative;
                    height: 300px;
                    background: #c8c8c8;
                    overflow: hidden;
                    border: 1px solid #cecece;
                }

                .cover-image img{
                    position: absolute;
                    top: 50%;
                    transform:translateY(-50%);
                    width: 100%;
                    left: 0px;
                }

                .cover-info .picture{
                    padding: 5px;
                    background: white;
                    border: 1px solid #cecece;
                    position: relative;
                }

                .cover-info .picture img{
                    width: 100%;
                }

                .cover-info{
                }

                .cover-info img{
                    border:none!important;
                }
            </style>

                    <div class="row text-center">
                        <div class="col-md-12 cover-info">
                            <div class="cover-image">
                                <img src="{{ $company->company_logo }}" alt="{{ $company->company_name}}" />
                            </div>
                            <div class="row cover-info" id="openings-title" style="margin:0px; margin-top:-100px;">
                                <div class="col-sm-2">
                                    <div class="picture">
                                        <div class="photo-wrapper">
                                            <img src="{{asset('img/bg-img.png')}}" class="bg-img">
                                            <img class="_image" src="{{ $company->company_logo }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row" style="text-align:left;">
                                        <div class="col-sm-6">
                                            <h1>
                                                <a href="{{ url('companies', $company['id']) }}">
                                                    {{ $company->company_name }}
                                                    <br>
                                                </a>
                                            </h1>
                                        </div>
                                        <div class="col-sm-6">
                                            <h5>
                                                <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                                                &nbsp; {{ $company->city }}, {{ $company->country }}
                                            </h5>
                                            <h5>
                                                <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
                                                &nbsp; {{ $company->created_at }}
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


            <hr>
            <div class="row">
                <div class="col-md-8">
                    <h3>About us:</h3>
                    {{ $company->what }}
                </div>
                <div class="col-md-4">
                    <h3>Company details:</h3>
                    <ul class="company-list-info">
                        <li>
                            <div class="field-name">Email</div>
                            <div class="field-value">{{ $company->email }}</div>
                        </li>
                        <li>
                            <div class="field-name">CEO</div>
                            <div class="field-value">{{ $company->ceo_name }}</div>
                        </li>
                        <li>
                            <div class="field-name">COO</div>
                            <div class="field-value">{{ $company->in_charge }}</div>
                        </li>
                        <li>
                            <div class="field-name">Address(s)</div>
                            <div class="field-value">{{ $company->address1 }}</div>
                            <div class="field-value">{{ $company->address2 }}</div>
                        </li>
                        <li>
                            <div class="field-name">City</div>
                            <div class="field-value">{{ $company->city }}</div>
                        </li>
                        <li>
                            <div class="field-name">Country</div>
                            <div class="field-value">{{ $company->country }}</div>
                        </li>
                        <li>
                            <div class="field-name">Website</div>
                            <div class="field-value">{{ $company->url }}</div>
                        </li>
                        <li>
                            <div class="field-name">Contact</div>
                            <div class="field-value">{{ $company->tel }} (Tel)</div>
                        </li>
                        <li>
                            <div class="field-name">Employees</div>
                            <div class="field-value">{{ $company->number_of_employee }}</div>
                        </li>
                    </ul>
                </div>
            </div>

            @if (!Auth::guest())
                @if (Auth::user()->role == 1)
                    @if (in_array(Auth::user()->id, $companies_ids))
                        <br/>
                        {!! link_to(action('CompaniesController@edit', [$company->id]), '編集', ['class' => 'btn btn-primary']) !!}
                        <br/>
                        {!! Form::open(['method' => 'DELETE', 'url' => ['companies', $company->id]]) !!}
                        {!! Form::submit('削除', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    @endif
                @endif
            @endif




    </div> {{-- END COMPANY INFO --}}

    <div id="joblists" class="tab-pane fade">
      <h3>Opening Job lists</h3>
        <div class="row">
            <div class="col-md-10">
                   @if (count($openings) > 0)
                    @foreach ($openings as $opening)
                        <div class="job-tile">
                            <div>
                                <span class="job-position featured">Featured</span>
                                <span class="job-position regular">Regular</span>
                                <span class="job-position intern">Intern</span>
                            </div>
                            <div class="job-title">
                                <a href="{{ url('openings', $opening->id) }}"> {{ $opening->title }} </a>
                                <img class="pull-right" src="/storage/{{ $opening->company->company_logo }}" alt="" border="0" height="100" width="130" style="max-width: 130px;">
                            </div>
                            <div class="company-name_opening_list"><a href="{{ url('companies', $opening->company->id) }}"> {{$opening->company->company_name}} </a>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <ul class="opening-feature-info-list">
                                        <li>
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            {{ $opening->company->address1 }}
                                        </li>
                                        <li>
                                            <i class="fa fa-dollar" aria-hidden="true"></i>
                                            PHP 10,000 - 15,000
                                        </li>                                                                
                                        <li>
                                            <i class="fa fa-code" aria-hidden="true"></i>
                                            <div class="label label-warning">
                                                Java
                                            </div>
                                            <div class="label label-primary">
                                                Python
                                            </div>
                                            <div class="label label-info">
                                                <div class="hover-info">
                                                    <div class="pointer"></div>
                                                    <div class="content">
                                                        <ul>
                                                            <li>JQuery</li>
                                                            <li>Angular</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                Javascript
                                            </div>
                                            <div class="label label-info">
                                                HTML
                                            </div>
                                        </li>
                                    </ul>
                                </div> <!-- col-sm-6 -->

                                <div class="col-sm-4">
                                    <ul class="opening-feature-info-list">
                                        <li>
                                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                            {{ $opening->requirements }}
                                        </li>
                                        <li>
                                            <i class="fa fa-file-text" aria-hidden="true"></i>
                                            {{ $opening->details }}
                                        </li>                                         
                                    </ul>
                                </div> <!-- col-sm-6 -->
                            </div>
                            <hr class="opening-top-date-hr" style="margin-top: 7px; margin-bottom: 7px;">
                            <div class="footer">
                                <div class="pull-left">
                                    <div class="foggy-text"> {{ date(' M. j, Y ',strtotime($opening->created_at)) }} </div>
                                </div>
                                <div class="pull-right">
                                    <div class="foggy-text">
                                        @include('openings.opening_bookmark.bookmark_button', ['opening' => $opening])
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="col-md-2 well">
                <h4>Advertisement</h4>
            </div>
        </div> {{-- END OF ROW --}}

        </div>

      </div>

    </div>
</div>
@endsection
