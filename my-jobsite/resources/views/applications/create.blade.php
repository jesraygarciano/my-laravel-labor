@extends('layouts.app')

@section('content')

    <div id="single-opening" class="container">
        <ul class="nav nav-pills">
            <li class="active"><a data-toggle="pill" href="#application_form">Application Form</a></li>
            <li><a data-toggle="pill" href="#jobinfo">Job Information</a></li>
            <li><a data-toggle="pill" href="#companyinfo">Company Information</a></li>
        </ul>
        <div class="tab-content">

{{--
 ####  #####  #####    ###### ####  #####  ##   ##
##  ## ##  ## ##  ##   ##    ##  ## ##  ## ### ###
##  ## ##  ## ##  ##   ##    ##  ## ##  ## #######
###### #####  #####    ##### ##  ## #####  ## # ##
##  ## ##     ##       ##    ##  ## ## ##  ## # ##
##  ## ##     ##       ##    ##  ## ##  ## ##   ##
##  ## ##     ##       ##     ####  ##  ## ##   ##
--}}

            <div id="application_form" class="tab-pane fade in active"> {{-- START JOB INFO --}}
                @include('inc.message')
                {!! Form::open(['action' => 'ApplicationController@store','method' => 'POST']) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    {!! Form::hidden('opening_id', $opening->id) !!}
                    <div class="form-row">
                        <h3>Application form:</h3>
                        <div class="form-group col-md-12">
                            {!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
                            {!! Form::textarea('description', old('description'), ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-row">
                            <h3>Resume information:</h3>
                            <div class="form-group col-md-4">
                                {!!Form::label('f_name', 'First Name:')!!}
                                {!!Form::text('f_name', $resume->f_name, ['class' => 'form-control'])!!}
                            </div>

                            <div class="form-group col-md-4">
                                {!!Form::label('m_name', 'Middle Name:')!!}
                                {!!Form::text('m_name', $resume->m_name, ['class' => 'form-control'])!!}
                            </div>
                            <div class="form-group col-md-4">
                                {!!Form::label('l_name', 'Last Name:')!!}
                                {!!Form::text('l_name', $resume->l_name, ['class' => 'form-control'])!!}
                            </div>
                    </div>

                    <div class="form-row">
                            <div class="form-group col-md-4">
                                {!!Form::label('email', 'Email address:')!!}
                                {!!Form::text('email', $resume->email, ['class' => 'form-control'])!!}
                            </div>

                            <div class="form-group col-md-4">
                                {!!Form::label('birth_date', 'Date of birth:')!!}
                                {!!Form::date('birth_date', $resume->birth_date, ['class' => 'form-control'])!!}
                            </div>
                            <div class="form-group col-md-4">
                                {!!Form::label('civil_status', 'Civil Status:')!!}
                                {!!Form::text('civil_status', $resume->civil_status, ['class' => 'form-control'])!!}
                            </div>
                    </div>

                    <div class="form-row">
                            <div class="form-group col-md-4">
                                {!!Form::label('nationality', 'Nationality:')!!}
                                {!!Form::text('nationality', $resume->nationality, ['class' => 'form-control'])!!}
                            </div>

                            <div class="form-group col-md-4">
                                {!!Form::label('country', 'Country:')!!}
                                {!!Form::text('country', $resume->country, ['class' => 'form-control'])!!}
                            </div>
                            <div class="form-group col-md-4">
                                {!!Form::label('city', 'City:')!!}
                                {!!Form::text('city', $resume->city, ['class' => 'form-control'])!!}
                            </div>
                    </div>

                    <div class="form-row">
                            <div class="form-group col-md-6">
                                {!!Form::label('address1', 'Current Address:')!!}
                                {!!Form::text('address1', $resume->address1, ['class' => 'form-control'])!!}
                            </div>

                            <div class="form-group col-md-6">
                                {!!Form::label('address2', 'Permanent Address:')!!}
                                {!!Form::text('address2', $resume->address2, ['class' => 'form-control'])!!}
                            </div>
                    </div>

                    <div class="form-row">
                            <div class="form-group col-md-4">
                                {!!Form::label('postal', 'Postal Code:')!!}
                                {!!Form::text('postal', $resume->postal, ['class' => 'form-control'])!!}
                            </div>

                            <div class="form-group col-md-4">
                                {!!Form::label('phone_number', 'Phone number:')!!}
                                {!!Form::text('phone_number', $resume->phone_number, ['class' => 'form-control'])!!}
                            </div>
                            <div class="form-group col-md-4">
                                {!!Form::label('gender', 'Gender:')!!}
                                {!!Form::text('gender', $resume->gender, ['class' => 'form-control'])!!}
                            </div>
                    </div>

                    <div class="form-group col-md-12">
                        {!!Form::label('objective', 'Objective:')!!}
                        {!!Form::textarea('objective', $resume->objective, ['class' => 'form-control'])!!}
                    </div>

                    <div class="form-row">
                            <div class="form-group col-md-4">
                                {!!Form::label('university', 'University:')!!}
                                {!!Form::text('university', $resume->university, ['class' => 'form-control'])!!}
                            </div>

                            <div class="form-group col-md-4">
                                {!!Form::label('field_of_study', 'Field of study:')!!}
                                {!!Form::text('field_of_study', $resume->field_of_study, ['class' => 'form-control'])!!}
                            </div>
                            <div class="form-group col-md-4">
                                {!!Form::label('program_of_study', 'Program of study:')!!}
                                {!!Form::text('program_of_study', $resume->program_of_study, ['class' => 'form-control'])!!}
                            </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                                <div class="form-group">
                                    {!!Form::label('term', 'Numbers of Term:')!!}
                                    <br />
                                    {!!Form::selectRange('term', 1,20, ['class' => 'field'])!!}
                                    {!!Form::select('term', ["day", "week", "month", "year"], null, ['class' => 'field'])!!}
                                </div>
                        </div>

                        <div class="form-group col-md-9">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('skills[]', 'PHP:')!!}
                                    <div class="" style="margin-bottom:5px;">
                                        @if (in_array(1, $languages_ids))
                                            {!!Form::checkbox('skills[]', 1, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 1, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                                        @endif

                                        @if (in_array(2, $languages_ids))
                                            {!!Form::checkbox('skills[]', 2, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;laravel&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 2, null)!!}&nbsp;&nbsp;laravel&nbsp;&nbsp;&nbsp;
                                        @endif

                                        @if (in_array(3, $languages_ids))
                                            {!!Form::checkbox('skills[]', 3, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;cake&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 3, null)!!}&nbsp;&nbsp;cake&nbsp;&nbsp;&nbsp;
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!!Form::label('skills[]', 'Ruby:')!!}
                                    <div class="" style="margin-bottom:5px;">
                                        @if (in_array(4, $languages_ids))
                                            {!!Form::checkbox('skills[]', 4, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 4, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                                        @endif

                                        @if (in_array(5, $languages_ids))
                                            {!!Form::checkbox('skills[]', 5, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;rails&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 5, null)!!}&nbsp;&nbsp;rails&nbsp;&nbsp;&nbsp;
                                        @endif

                                        @if (in_array(6, $languages_ids))
                                            {!!Form::checkbox('skills[]', 6, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 6, null)!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="form-group">
                                    {!!Form::label('skills[]', 'JAVA:')!!}
                                    <div class="" style="margin-bottom:5px;">
                                        @if (in_array(7, $languages_ids))
                                            {!!Form::checkbox('skills[]', 7, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 7, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                                        @endif

                                        @if (in_array(8, $languages_ids))
                                            {!!Form::checkbox('skills[]', 8, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;struts&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 8, null)!!}&nbsp;&nbsp;struts&nbsp;&nbsp;&nbsp;
                                        @endif

                                        @if (in_array(9, $languages_ids))
                                            {!!Form::checkbox('skills[]', 9, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;jsf&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 9, null)!!}&nbsp;&nbsp;jsf&nbsp;&nbsp;&nbsp;
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('skills[]', 'python:')!!}
                                    <div class="" style="margin-bottom:5px;">
                                        @if (in_array(10, $languages_ids))
                                            {!!Form::checkbox('skills[]', 10, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 10, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                                        @endif

                                        @if (in_array(11, $languages_ids))
                                            {!!Form::checkbox('skills[]', 11, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;bottle&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 11, null)!!}&nbsp;&nbsp;bottle&nbsp;&nbsp;&nbsp;
                                        @endif

                                        @if (in_array(12, $languages_ids))
                                            {!!Form::checkbox('skills[]', 12, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                                        @else
                                            {!!Form::checkbox('skills[]', 12, null)!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        {!!Form::submit('Submit application', ['class' => 'btn btn-primary form-control'])!!}
                    </div>
                {!!Form::close()!!}
            </div>
{{--
##  ## ###### #####  ###### ##  ##  ####    ###### ##  ## ###### ####
##  ##   ##   ##  ##   ##   ##  ## ##  ##     ##   ##  ## ##    ##  ##
##  ##   ##   ##  ##   ##   ### ## ##         ##   ### ## ##    ##  ##
######   ##   #####    ##   ###### ## ###     ##   ###### ##### ##  ##
##  ##   ##   ## ##    ##   ## ### ##  ##     ##   ## ### ##    ##  ##
##  ##   ##   ##  ##   ##   ##  ## ##  ##     ##   ##  ## ##    ##  ##
##  ## ###### ##  ## ###### ##  ##  ####    ###### ##  ## ##     ####
--}}

            <div id="jobinfo" class="tab-pane fade"> {{-- START JOB INFO --}}
                <h3>Opening Information</h3>
                <div class="container" id="show_opening"> {{-- START OF SHOW OPENING --}}
                    <div class="row text-center">
                        <div class="col-md-12">
                            <img src="{{ asset('img/opening_banner.png') }}" />
                        </div>
                    </div>
                    <div class="row" id="openings-title">
                        <div class="col-md-2">
                            <img src="/storage/{{ $company->company_logo }}" alt="{{ $company-> company_name}}" />
                        </div>
                        <div class="col-md-6">
                            <h1>{{ $opening->title }}</h1>
                            <h4>
                                <a href="{{ url('companies', $company['id']) }}">
                                    {{ $company->company_name }}
                                    <br>
                                </a>
                            </h4>
                        </div>
                        <div class="col-md-4">
                            <h5>
                                <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                                &nbsp; {{ $opening->address }}
                            </h5>
                            <h5>
                                <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
                                &nbsp; {{ $opening->created_at }}
                            </h5>
                        </div>
                    </div>
                    <div class="row" id="openings-body">
                        <div class="col-md-7">
                            <div class="job-description">
                                <h4>
                                    <i class="fa fa-file-text" aria-hidden="true"></i>
                                    Job description:
                                </h4>
                                <hr class="hr-desc" />
                                {{ $opening->details }}
                                {{ $opening->details }}
                                {{ $opening->details }}
                                {{ $opening->details }}
                                {{ $opening->details }}
                            </div>
                            <div class="job-qualify">
                                <h4>
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                    Job Qualifications:
                                </h4>
                                <hr />
                                <h5>{{ $opening->requirements }}</h5>
                                <h5>{{ $opening->requirements }}</h5>
                                <h5>{{ $opening->requirements }}</h5>
                            </div>

                        </div>

                        <div class="col-md-5">
                            <h4>
                                <i class="fa fa-briefcase" aria-hidden="true"></i>
                                Company Profile:
                            </h4>
                            <hr>

                            <div class="row">
                                <div class="col-xs-6">
                                    Company size:
                                    <h5>
                                        <b>
                                            {{ $company->number_of_employee }}
                                        </b>
                                        Employees
                                    </h5>
                                    Telephone no.
                                    <h5>
                                        <b>
                                            {{ $company->tel }}
                                        </b>
                                    </h5>
                                    Country
                                    <h5>
                                        <b>
                                            {{ $company->country }}
                                        </b>
                                    </h5>
                                </div>
                                <div class="col-xs-6">
                                    Postal no.
                                    <h5>
                                        <b>
                                            {{ $company->postal }}
                                        </b>
                                    </h5>
                                    CEO:
                                    <h5>
                                        <b>
                                            {{ $company->ceo_name }}
                                        </b>
                                    </h5>
                                    Bill company name:
                                    <h5>
                                        <b>
                                            {{ $company->bill_company_name }}
                                        </b>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    {!! link_to(action('ApplicationController@create', [$opening->id]), 'Application Form', ['class' => 'btn btn-danger']) !!}
                </div>
            </div>

{{--
 ####   ####  ##   ## #####   ####  ##  ## ##  ##
##  ## ##  ## ### ### ##  ## ##  ## ##  ## ##  ##
##     ##  ## ####### ##  ## ##  ## ### ## ##  ##
##     ##  ## ## # ## #####  ###### ######  ####
##     ##  ## ## # ## ##     ##  ## ## ###   ##
##  ## ##  ## ##   ## ##     ##  ## ##  ##   ##
 ####   ####  ##   ## ##     ##  ## ##  ##   ##
--}}

            <div id="companyinfo" class="tab-pane fade">
                <h3>Company Information</h3>
                <div class="row">
                    <div class="col-md-4">
                        <img src="/storage/{{ $company->company_logo }}" alt="{{ $company-> company_name}}" />                        
                    </div>
                    <div class="col-md-6">
                        <h1>{{ $company->company_name }}</h1>
                        <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                        <span style="font-size:16px;margin-left:1rem;">
                            <b>{{ $company['number_of_employee'] }} Employees
                                <br />
                                <br />
                                {{ $company->address1 }}
                                <br />
                                {{ $company->email }}
                            </b>
                        </span>
                    </div>
                    <div class="col-md-2">
                        <h3>{{ $company->city }}</h3>
                    </div>
                </div>
                <hr>
                <h3>About us:</h3>
                {{ $company->what }}
                <h3>Company details:</h3>
                <div class = "body">email : {{ $company->email }}</div>
                <div class = "body">in_charge : {{ $company->in_charge }}</div>
                <div class = "body">ceo_name : {{ $company->ceo_name }}</div>
                <div class = "body">address1 : {{ $company->address1 }}</div>
                <div class = "body">address2 : {{ $company->address2 }}</div>
                <div class = "body">city : {{ $company->city }}</div>
                <div class = "body">country : {{ $company->country }}</div>
                <div class = "body">url : {{ $company->url }}</div>
                <div class = "body">tel : {{ $company->tel }}</div>
                <div class = "body">number_of_employee : {{ $company->number_of_employee }}</div>
            </div>
        </div>
    </div>
@endsection
