@extends('layouts.app')

@section('content')

<div class="container">
@include('inc.message')
@include('errors.form_errors')
    <h1>Updating your resume</h1>

    <hr/>

    {{-- {!!Form::open(['action' => 'ResumesController@update', 'method' => 'POST']) !!} --}}
    {!!Form::model($resume, ['action' => ['ResumesController@update', $resume->id], 'method' => 'PATCH']) !!}
        {!! Form::hidden('user_id', Auth::id()) !!}
        <div class="form-row">
                <div class="form-group col-md-4">
                    {!!Form::label('f_name', 'First Name:')!!}
                    {!!Form::text('f_name', null, ['class' => 'form-control'])!!}
                </div>

                <div class="form-group col-md-4">
                    {!!Form::label('m_name', 'Middle Name:')!!}
                    {!!Form::text('m_name', null, ['class' => 'form-control'])!!}
                </div>
                <div class="form-group col-md-4">
                    {!!Form::label('l_name', 'Last Name:')!!}
                    {!!Form::text('l_name', null, ['class' => 'form-control'])!!}
                </div>
        </div>

        <div class="form-row">
                <div class="form-group col-md-4">
                    {!!Form::label('email', 'Email address:')!!}
                    {!!Form::text('email', null, ['class' => 'form-control'])!!}
                </div>

                <div class="form-group col-md-4">
                    {!!Form::label('birth_date', 'Date of birth:')!!}
                    {!!Form::date('birth_date', null, ['class' => 'form-control'])!!}
                </div>
                <div class="form-group col-md-4">
                    {!!Form::label('civil_status', 'Civil Status:')!!}
                    {!!Form::text('civil_status', null, ['class' => 'form-control'])!!}
                </div>
        </div>

        <div class="form-row">
                <div class="form-group col-md-4">
                    {!!Form::label('nationality', 'Nationality:')!!}
                    {!!Form::text('nationality', null, ['class' => 'form-control'])!!}
                </div>

                <div class="form-group col-md-4">
                    {!!Form::label('country', 'Country:')!!}
                    {!!Form::text('country', null, ['class' => 'form-control'])!!}
                </div>
                <div class="form-group col-md-4">
                    {!!Form::label('city', 'City:')!!}
                    {!!Form::text('city', null, ['class' => 'form-control'])!!}
                </div>
        </div>

        <div class="form-row">
                <div class="form-group col-md-6">
                    {!!Form::label('address1', 'Current Address:')!!}
                    {!!Form::text('address1', null, ['class' => 'form-control'])!!}
                </div>

                <div class="form-group col-md-6">
                    {!!Form::label('address2', 'Permanent Address:')!!}
                    {!!Form::text('address2', null, ['class' => 'form-control'])!!}
                </div>
        </div>

        <div class="form-row">
                <div class="form-group col-md-4">
                    {!!Form::label('postal', 'Postal Code:')!!}
                    {!!Form::text('postal', null, ['class' => 'form-control'])!!}
                </div>

                <div class="form-group col-md-4">
                    {!!Form::label('phone_number', 'Phone number:')!!}
                    {!!Form::text('phone_number', null, ['class' => 'form-control'])!!}
                </div>
                <div class="form-group col-md-4">
                    {!!Form::label('gender', 'Gender:')!!}
                    {!!Form::text('gender', null, ['class' => 'form-control'])!!}
                </div>
        </div>

        <div class="form-group col-md-12">
            {!!Form::label('objective', 'Objective:')!!}
            {!!Form::textarea('objective', null, ['class' => 'form-control'])!!}
        </div>

        <div class="form-row">
                <div class="form-group col-md-4">
                    {!!Form::label('university', 'University:')!!}
                    {!!Form::text('university', null, ['class' => 'form-control'])!!}
                </div>

                <div class="form-group col-md-4">
                    {!!Form::label('field_of_study', 'Field of study:')!!}
                    {!!Form::text('field_of_study', null, ['class' => 'form-control'])!!}
                </div>
                <div class="form-group col-md-4">
                    {!!Form::label('program_of_study', 'Program of study:')!!}
                    {!!Form::text('program_of_study', null, ['class' => 'form-control'])!!}
                </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-3">
                    <div class="form-group">
                        {!!Form::label('term', 'Numbers of Term:')!!}
                        <br />
                        {!!Form::selectRange('term', 1,20, ['class' => 'field'])!!}
                        {!!Form::select('term', ["day", "week", "month", "year"], null, ['class' => 'field'])!!}
                    </div>
            </div>

            <div class="form-group col-md-9">
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('skills[]', 'PHP:')!!}
                        <div class="" style="margin-bottom:5px;">
                            @if (in_array(1, $languages_ids))
                                {!!Form::checkbox('skills[]', 1, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 1, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                            @endif

                            @if (in_array(2, $languages_ids))
                                {!!Form::checkbox('skills[]', 2, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;laravel&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 2, null)!!}&nbsp;&nbsp;laravel&nbsp;&nbsp;&nbsp;
                            @endif

                            @if (in_array(3, $languages_ids))
                                {!!Form::checkbox('skills[]', 3, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;cake&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 3, null)!!}&nbsp;&nbsp;cake&nbsp;&nbsp;&nbsp;
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!!Form::label('skills[]', 'Ruby:')!!}
                        <div class="" style="margin-bottom:5px;">
                            @if (in_array(4, $languages_ids))
                                {!!Form::checkbox('skills[]', 4, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 4, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                            @endif

                            @if (in_array(5, $languages_ids))
                                {!!Form::checkbox('skills[]', 5, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;rails&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 5, null)!!}&nbsp;&nbsp;rails&nbsp;&nbsp;&nbsp;
                            @endif

                            @if (in_array(6, $languages_ids))
                                {!!Form::checkbox('skills[]', 6, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 6, null)!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                            @endif
                        </div>
                    </div>

                </div>

                <div class="col-md-6">

                    <div class="form-group">
                        {!!Form::label('skills[]', 'JAVA:')!!}
                        <div class="" style="margin-bottom:5px;">
                            @if (in_array(7, $languages_ids))
                                {!!Form::checkbox('skills[]', 7, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 7, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                            @endif

                            @if (in_array(8, $languages_ids))
                                {!!Form::checkbox('skills[]', 8, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;struts&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 8, null)!!}&nbsp;&nbsp;struts&nbsp;&nbsp;&nbsp;
                            @endif

                            @if (in_array(9, $languages_ids))
                                {!!Form::checkbox('skills[]', 9, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;jsf&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 9, null)!!}&nbsp;&nbsp;jsf&nbsp;&nbsp;&nbsp;
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        {!!Form::label('skills[]', 'python:')!!}
                        <div class="" style="margin-bottom:5px;">
                            @if (in_array(10, $languages_ids))
                                {!!Form::checkbox('skills[]', 10, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 10, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;
                            @endif

                            @if (in_array(11, $languages_ids))
                                {!!Form::checkbox('skills[]', 11, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;bottle&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 11, null)!!}&nbsp;&nbsp;bottle&nbsp;&nbsp;&nbsp;
                            @endif

                            @if (in_array(12, $languages_ids))
                                {!!Form::checkbox('skills[]', 12, null, ['checked' => 'checked'])!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                            @else
                                {!!Form::checkbox('skills[]', 12, null)!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="form-group col-md-12">
            {!!Form::submit('Update Your Resume', ['class' => 'btn btn-primary form-control'])!!}
        </div>
    {!!Form::close()!!}
</div>

@endsection
