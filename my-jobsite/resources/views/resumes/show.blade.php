@extends('layouts.app')

@section('content')

@include('inc.message')

<div class="container">
    <div class="resume-info">
        <div class="row">
            <div class="col-md-12">
                <h3 id="resume-info-title">Resume information </h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="photo col-md-offset-2 col-md-3">
                                <div style="background:red; width: 200px; height: 200px; display: inline-block; text-align: center;">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <h2>
                                    {{ $resume->f_name }}
                                    {{ $resume->m_name }}
                                    {{ $resume->l_name }}
                                </h2>
                                <h4>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-{{ $resume->address1 }}
                                    {{ $resume->address2 }}
                                    {{ $resume->city }}
                                    {{ $resume->country }}
                                </h4>
                                <h4>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-{{ $resume->phone_number }}
                                </h4>
                                <h4>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-{{ $resume->email }}
                                </h4>
                            </div>
                        </div>
                        <h4 class="resume-info-sub-title">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            Personal Information
                        </h4>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Age</label>
                            <div class="col-sm-10">
                                {{ $age }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Birth date</label>
                            <div class="col-sm-10">
                                {{ $birth_date }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Gender</label>
                            <div class="col-sm-10">
                                {{ $resume->l_name }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Religion</label>
                            <div class="col-sm-10">
                                {{ $resume->birth_date }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Marital Status</label>
                            <div class="col-sm-10">
                                {{ $resume->birth_date }}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h4 class="resume-info-sub-title">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            Education
                        </h4>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">University</label>
                            <div class="col-sm-10">
                                {{ $resume->university }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Field of study</label>
                            <div class="col-sm-10">
                                {{ $resume->field_of_study }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Program of study</label>
                            <div class="col-sm-10">
                                {{ $resume->program_of_study }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Schoool year attended</label>
                            <div class="col-sm-10">
                                {{ $resume->university_enter_month }}
                                {{ $resume->university_enter_year }}
                                &nbsp;&nbsp; - &nbsp;&nbsp;
                                {{ $resume->university_graduate_month }}
                                {{ $resume->university_graduate_year }}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h4 class="resume-info-sub-title">
                            <i class="fa fa-trophy" aria-hidden="true"></i>
                            Experience
                        </h4>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Work experience</label>
                            <div class="col-sm-10">
                                {{ $resume->experience }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Other work experience</label>
                            <div class="col-sm-10">
                                {{ $resume->objective }}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h4 class="resume-info-sub-title">
                            <i class="fa fa-code" aria-hidden="true"></i>
                            Programming language
                        </h4>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">PHP</label>
                            <div class="col-sm-10">
                                {{ $skill_languages['php'] }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ruby</label>
                            <div class="col-sm-10">
                                {{ $skill_languages['ruby'] }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Java</label>
                            <div class="col-sm-10">
                                {{ $skill_languages['java'] }}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">python</label>
                            <div class="col-sm-10">
                                {{ $skill_languages['python'] }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div style="margin-top:10px; margin-bottom:10px;">
        {!! link_to(action('ResumesController@edit', $resume->id) , 'update', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@endsection
