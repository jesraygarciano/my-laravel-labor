@extends('layouts.app')

@section('content')

<div class="container">
@include('inc.message')
@include('errors.form_errors')
    <h1>Creating your resume</h1>

    <hr/>

    {!!Form::open(['action' => 'ResumesController@store', 'method' => 'POST']) !!}
        {!! Form::hidden('user_id', Auth::id()) !!}

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('f_name', 'First Name:')!!}
                    {!!Form::text('f_name', $user->f_name, ['class' => 'form-control'])!!}
                </div>
                <div class="col-md-4">
                    {!!Form::label('m_name', 'Middle Name:')!!}
                    {!!Form::text('m_name', $user->m_name, ['class' => 'form-control'])!!}
                </div>
                <div class="col-md-4">
                    {!!Form::label('l_name', 'Last Name:')!!}
                    {!!Form::text('l_name', $user->l_name, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('phone_number', 'Phone number:')!!}
                    {!!Form::text('phone_number', null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('email', 'Email address:')!!}
                    {!!Form::text('email', $user->email, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                {!!Form::label('birth_date', 'Date of birth:')!!}
                {!!Form::date('birth_date', null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('address1', 'Address1:')!!}
                    {!!Form::text('address1', null, ['class' => 'form-control'])!!}
                </div>

                <div class="col-md-4">
                    {!!Form::label('address2', 'Address2:')!!}
                    {!!Form::text('address2', null, ['class' => 'form-control'])!!}
                </div>

                <div class="col-md-4">
                    {!!Form::label('city', 'City:')!!}
                    {!!Form::text('city', null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('country', 'Country:')!!}
                    {!!Form::text('country', null, ['class' => 'form-control'])!!}
                </div>

                <div class="col-md-4">
                    {!!Form::label('postal', 'Postal:')!!}
                    {!!Form::text('postal', null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('nationality', 'Nationality:')!!}
                    {!!Form::text('nationality', null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('gender', 'Gender:')!!}<br>
                    {!!Form::radio('gender', 'male')!!}&nbsp;&nbsp;male&nbsp;&nbsp;&nbsp;&nbsp;
                    {!!Form::radio('gender', 'female', true)!!}&nbsp;&nbsp;female
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('religion', 'Religion:')!!}
                    {!!Form::text('religion', null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('civil_status', 'Marital Status:')!!}<br>
                    {!!Form::radio('civil_status', 'single')!!}&nbsp;&nbsp;single&nbsp;&nbsp;&nbsp;&nbsp;
                    {!!Form::radio('civil_status', 'married', true)!!}&nbsp;&nbsp;married
                </div>
            </div>
        </div>


        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    {!!Form::label('university', 'University:')!!}
                    {!!Form::text('university', null, ['class' => 'form-control'])!!}
                </div>

                <div class="col-md-4">
                    {!!Form::label('field_of_study', 'Field of study:')!!}
                    {!!Form::text('field_of_study', null, ['class' => 'form-control'])!!}
                </div>
                <div class="col-md-4">
                    {!!Form::label('program_of_study', 'Program of study:')!!}
                    {!!Form::text('program_of_study', null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
             <div class="row">
                <div class="col-md-8">
                    {!!Form::label('duration', 'Duration:')!!}<br>
                    {!!Form::select('term', $month_array, ['class' => 'field'])!!},
                    {!!Form::selectRange('term', 1940,2018, ['class' => 'field'])!!}
                    &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;
                     {!!Form::select('term', $month_array, ['class' => 'field'])!!},
                    {!!Form::selectRange('term', 1940,2018, ['class' => 'field'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    {!!Form::label('objective', 'Objective:')!!}
                    {!!Form::textarea('objective', null, ['class' => 'form-control'])!!}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-6">

                    {!!Form::label('skills[]', 'PHP:')!!}
                    <div class="" style="margin-bottom:5px;">
                        {!!Form::checkbox('skills[]', 1, null, ['class' => 'custom-control-input'])!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;

                        {!!Form::checkbox('skills[]', 2, null)!!}&nbsp;&nbsp;laravel&nbsp;&nbsp;&nbsp;

                        {!!Form::checkbox('skills[]', 3, null)!!}&nbsp;&nbsp;cake&nbsp;&nbsp;&nbsp;
                    </div>

                    {!!Form::label('skills[]', 'JAVA:')!!}
                    <div class="" style="margin-bottom:5px;">
                        {!!Form::checkbox('skills[]', 4, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;

                        {!!Form::checkbox('skills[]', 5, null)!!}&nbsp;&nbsp;struts&nbsp;&nbsp;&nbsp;

                        {!!Form::checkbox('skills[]', 6, null)!!}&nbsp;&nbsp;jsf&nbsp;&nbsp;&nbsp;
                    </div>
                </div>

                <div class="col-md-6">
                    {!!Form::label('skills[]', 'Ruby:')!!}
                    <div class="" style="margin-bottom:5px;">
                        {!!Form::checkbox('skills[]', 7, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;

                        {!!Form::checkbox('skills[]', 8, null)!!}&nbsp;&nbsp;rails&nbsp;&nbsp;&nbsp;

                        {!!Form::checkbox('skills[]', 9, null)!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                    </div>

                    {!!Form::label('skills[]', 'python:')!!}
                    <div class="" style="margin-bottom:5px;">
                        {!!Form::checkbox('skills[]', 10, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;

                        {!!Form::checkbox('skills[]', 11, null)!!}&nbsp;&nbsp;bottle&nbsp;&nbsp;&nbsp;

                        {!!Form::checkbox('skills[]', 12, null)!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>
        </div>

        <div class="ui sub header">Selection</div>
        <select multiple="" name="skills" class="ui fluid normal dropdown">
          <option value="">Skills</option>
        <option value="angular">Angular</option>
        <option value="css">CSS</option>
        <option value="design">Graphic Design</option>
        <option value="ember">Ember</option>
        <option value="html">HTML</option>
        <option value="ia">Information Architecture</option>
        <option value="javascript">Javascript</option>
        <option value="mech">Mechanical Engineering</option>
        <option value="meteor">Meteor</option>
        <option value="node">NodeJS</option>
        <option value="plumbing">Plumbing</option>
        <option value="python">Python</option>
        <option value="rails">Rails</option>
        <option value="react">React</option>
        <option value="repair">Kitchen Repair</option>
        <option value="ruby">Ruby</option>
        <option value="ui">UI Design</option>
        <option value="ux">User Experience</option>
        </select>
        {{ Request::path() }}
<!-- ADD Records -->

        <div class="form-row tech-skills">
            <h3>Advanced technical skills</h3>
            <div class="form-group col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('skills[]', 'Project manangement tools:')!!}
                        <div class="" style="margin-bottom:5px;">

                        <label id="sc-check" class="custom-control custom-checkbox">
                            {!!Form::checkbox('skills[]', 1, null, ['class' => 'custom-control-input cont-checklist'])!!}
                            <span class="custom-control-indicator cont-checklist-indicator"></span>
                            <span class="custom-control-description">Crocogile</span>
                        </label>

                        <label id="sc-check" class="custom-control custom-checkbox">
                            {!!Form::checkbox('skills[]', 2, null, ['class' => 'custom-control-input cont-checklist'])!!}
                            <span class="custom-control-indicator cont-checklist-indicator"></span>
                            <span class="custom-control-description">Jira</span>
                        </label>

                        <label id="sc-check" class="custom-control custom-checkbox">
                            {!!Form::checkbox('skills[]', 3, null, ['class' => 'custom-control-input cont-checklist'])!!}
                            <span class="custom-control-indicator cont-checklist-indicator"></span>
                            <span class="custom-control-description">Trello</span>
                        </label>

                        <label id="sc-check" class="custom-control custom-checkbox">
                            {!!Form::checkbox('skills[]', 4, null, ['class' => 'custom-control-input cont-checklist'])!!}
                            <span class="custom-control-indicator cont-checklist-indicator"></span>
                            <span class="custom-control-description">Blossom</span>
                        </label>

                        <label id="sc-check" class="custom-control custom-checkbox">
                            {!!Form::checkbox('skills[]', 5, null, ['class' => 'custom-control-input cont-checklist'])!!}
                            <span class="custom-control-indicator cont-checklist-indicator"></span>
                            <span class="custom-control-description">Zenhub</span>
                        </label>

                        <label id="sc-check" class="custom-control custom-checkbox">
                            {!!Form::checkbox('skills[]', 6, null, ['class' => 'custom-control-input cont-checklist'])!!}
                            <span class="custom-control-indicator cont-checklist-indicator"></span>
                            <span class="custom-control-description">Teamwork</span>
                        </label>                        


                        <label id="sc-check" class="custom-control custom-checkbox">
                            {!!Form::text('skills[]', 6, null, ['class' => 'custom-control-input cont-checklist'])!!}
                            <span class="custom-control-indicator cont-checklist-indicator"></span>
                            <span class="custom-control-description">Other specify</span>
                        </label>  

                        </div>

                    </div>


                    <div class="form-group">
                        {!!Form::label('skills[]', 'JAVA:')!!}
                        <div class="" style="margin-bottom:5px;">
                            {!!Form::checkbox('skills[]', 4, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;

                            {!!Form::checkbox('skills[]', 5, null)!!}&nbsp;&nbsp;struts&nbsp;&nbsp;&nbsp;

                            {!!Form::checkbox('skills[]', 6, null)!!}&nbsp;&nbsp;jsf&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('skills[]', 'Ruby:')!!}
                        <div class="" style="margin-bottom:5px;">
                            {!!Form::checkbox('skills[]', 7, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;

                            {!!Form::checkbox('skills[]', 8, null)!!}&nbsp;&nbsp;rails&nbsp;&nbsp;&nbsp;

                            {!!Form::checkbox('skills[]', 9, null)!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>

                    <div class="form-group">
                        {!!Form::label('skills[]', 'python:')!!}
                        <div class="" style="margin-bottom:5px;">
                            {!!Form::checkbox('skills[]', 10, null)!!}&nbsp;&nbsp;without framework&nbsp;&nbsp;&nbsp;

                            {!!Form::checkbox('skills[]', 11, null)!!}&nbsp;&nbsp;bottle&nbsp;&nbsp;&nbsp;

                            {!!Form::checkbox('skills[]', 12, null)!!}&nbsp;&nbsp;other&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {!! Form::hidden('is_active', 1) !!}
        {!! Form::hidden('is_master', 1) !!}


        <div class="form-group col-md-12">
            {!!Form::submit('Register Your Resume', ['class' => 'btn btn-primary form-control'])!!}
        </div>
    {!!Form::close()!!}
</div>s

@endsection
