@extends('layouts.app')

@section('content')
    
    <!-- inline css || uelmar -->
    <style type="text/css">
        .info-container{
            padding: 10px;
            white-space: initial;
        }
        .job-position{
            padding: 5px;
            background: white;
            border-radius: 3px;
            font-weight: bold;
            position: relative;
            left: 0px;
            top: 15px;
        }

        .featured{
            color: #eda637;
            border: 1px solid #eda637;
        }

        .regular{
            color: #3c763d;
            border: 1px solid #3c763d;
        }

        .intern{
            color: #79ace9;
            border: 1px solid #79ace9;
        }

        /*.job-position-list{
            margin:10px;
        }*/

        .feature-info-list{
            list-style: none;
            padding: 0px;
        }

        .feature-info-list li .label{
            position: relative;
            display: inline-block;
            cursor: pointer;
        }

        .feature-info-list li .info-icon {
            position: absolute;
            left: 0px;
        }

        .feature-info-list li .hover-info{
            position: absolute;
            top: 100%;
            left: 0px;
            margin-top: 5px;
            display: none;
            z-index: 1000;
        }

        .feature-info-list li .label:hover .hover-info{
            display: block;
        }

        .feature-info-list li .hover-info .pointer{
            position: absolute;
            bottom:100%;
            left: 10px;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 5px 5px 5px;
            border-color: transparent transparent #cecece transparent;
        }

        .feature-info-list li .hover-info .content{
            background: white;
            border:1px solid #cecece;
            color: white;
            padding: 5px;
            border-radius: 5px;
            background: #4d5355;
            width: 100px;
        }

        .feature-info-list li .hover-info .content ul{
            padding-left: 15px;
        }

        .feature-info-list li{
            padding-left: 20px;
            padding-right: 130px;
            white-space:normal;
            position: relative;
        }

        .li-code{
            /*border: 1px solid #000000;*/
            margin-top: 5px;
        }

        .info-icon_address_money:before{
            margin-left: 3px;
            padding-bottom: -50px;
        }

        .job-title{
            font-size: 20px;
            margin-top: 40px;
        }

        .company-name{
            /**/
            margin-bottom: 10px;
        }

        .company-name a{
            color: inherit;
        }

        .landing-cover{
            position: relative;
            overflow: hidden;
        }

        .landing-cover .bg-image{
            width: 100%;
        }

        .landing-cover .image{
            position: absolute;
            width: 100%;
            left: 50%;
            top:50%;
            transform: translateY(-50%) translateX(-50%);
            transition:1000ms ease all;
        }

        .landing-cover .shadow{
            position: absolute;
            left: 0px;
            top: 0px;
            height: 100%;
            width: 100%;
            -webkit-box-shadow: inset 0px -20px 20px 0px rgba(0, 0, 0, 0.64);
            transition: 1000ms ease all;
        }

        .landing-cover:hover .shadow{
            -webkit-box-shadow: inset 0px -30px 20px 0px rgba(0, 0, 0, 0.64);
        }

        .landing-cover:hover .image{
            transform: translateY(-50%) translateX(-50%) scale(1.1);
        }

        .xtext12{
            position: relative;
            font-weight: bold;
            font-size: 30px;
            color:white;
            padding-top: 15px;
            padding-bottom: 5px;            
            text-align: center;
            background: #066c9c;
        }

        @media(max-width: 800px){
            .landing-cover{
                height: initial;
            }
        }
    </style>

    <div class="general-hero" style="margin-top:-30px;">
                <div class="landing-cover">
                    <img class="bg-image" src="{{asset('img/bg-banner.png')}}">
                    <img class="image" src="https://images.unsplash.com/photo-1504384308090-c894fdcc538d?auto=format&fit=crop&w=1500&q=80">
                    <div class="shadow"></div>

                    <div class="container" id="general_search">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group" id="adv-search">
                                   <input type="text" id="general_search_placeholder" class="form-control"  placeholder="Search for jobs" />
                                    <div class="input-group-btn">
                                        <div class="btn-group" role="group">
                                            <div class="dropdown dropdown-lg">
                                                <button type="button" class="btn btn-default dropdown-toggle"       data-toggle="dropdown" aria-expanded="false">
                                                    Filter by &nbsp <span class="caret"></span>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                    <form class="form-horizontal" role="form">
                                                      <div class="form-group">
                                                        <label for="filter">Filter by </label>
                                                        <select class="form-control">
                                                            <option value="0" selected>Intern</option>
                                                            <option value="1">Featured</option>
                                                            <option value="2">Most popular</option>
                                                            <option value="3">Top rated</option>
                                                            <option value="4">Most applied</option>
                                                        </select>
                                                      </div>
                                                      <div class="form-group">
                                                        <label for="contain">Company</label>
                                                        <input class="form-control" type="text" />
                                                      </div>
                                                      <div class="form-group">
                                                        <label for="contain">Contains the words</label>
                                                        <input class="form-control" type="text" />
                                                      </div>
                                                      <button type="submit" class="btn btn-primary">
                                                        Advanced search
                                                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>

                                                      </button>
                                                    </form>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary">
                                                Search 
                                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        <div class="feature-post-carosel" id="feature-post-carosel">
            <div class="xtext12"> Featured Jobs </div>
            <div class="post-container">
                <div class="wing">
                    @foreach( $featured_openings as $opening)
                    <div class="post-item">
                        <div class="info-container">
                            <div class="job-position-list">
                                <span class="job-position featured">Featured</span>
                                <span class="job-position regular">Regular</span>
                                <span class="job-position intern">Intern</span>
                            </div>
                            <div class="job-title ellipsis">
                                <a href="{{ url('openings', $opening->id) }}"> {{ $opening->title }} </a>
                            </div>
                            <div class="company-name ellipsis"><a href="{{ url('companies', $opening->company->id) }}"> {{$opening->company->company_name}} </a></div>
                            <img class="pull-right" src="{{ $opening->company->company_logo }}" alt="" border="0" height="100" width="130" style="max-width: 130px;">
                            <ul class="feature-info-list">
                                <li>
                                    <div class="ellipsis">
                                        <i class="fa fa-map-marker info-icon info-icon_address_money" aria-hidden="true">
                                        </i> 
                                        <span>
                                            {{ $opening->company->address1 }}
                                        </span> 
                                    </li>
                                </li>
                                <li>
                                    <div class="ellipsis">
                                        <i class="fa fa-dollar info-icon info-icon_address_money" aria-hidden="true">
                                        </i> 
                                        <span>
                                            PHP 10,000 - 15,000
                                        </span> 
                                    </li>
                                </li>
                                <li class="li-code">
                                    <i class="fa fa-code info-icon" aria-hidden="true"></i>
                                    <div class="label label-warning">
                                        Java
                                    </div>
                                    <div class="label label-primary">
                                        Python
                                    </div>
                                    <div class="label label-info">
                                        <div class="hover-info">
                                            <div class="pointer"></div>
                                            <div class="content">
                                                <ul>
                                                    <li>JQuery</li>
                                                    <li>Angular</li>
                                                </ul>
                                            </div>
                                        </div>
                                        Javascript
                                    </div>
                                    <div class="label label-info">
                                        HTML
                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <ul class="indicators">
                <li class="active"><div class="indecator-item"></div></li>
                <li><div class="indecator-item"></div></li>
                <li><div class="indecator-item"></div></li>
                <li><div class="indecator-item"></div></li>
                <li><div class="indecator-item"></div></li>
            </ul>
            <!-- <div style="text-align: center;">
                <a href="{{ url('openings') }}" type="button" class="btn btn-info">See All</a>
            </div> -->
            <div class="carosel-controls">
                <div class="left"><i class="fa fa-chevron-left"></i></div>
                <div class="right"><i class="fa fa-chevron-right"></i></div>
            </div>
        </div>
    </div>
    <!-- initializing feature hiring post car0sel -->
    <script type="text/javascript">
        $(document).ready(function(){
            $('#feature-post-carosel').featuredPostCarosel();
        });
    </script>

	<div class="general_portal container" style="margin-bottom:100px;">

        <h3 id="or-search"><a href="#">Programming Languages</a></h3>
        <hr style="margin-top: 5px;">
        <ul class="ud-list">
            <li>
                <a href="{{route('search_opening_with_language').'?language=php'}}">
                    <div class="round-icon medium-round-icon" style="background: #8993be;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Antu_php.svg/500px-Antu_php.svg.png">
                    </div>
                    <b>Php</b>
                </a>
            </li>
            <li>
                <a href="{{route('search_opening_with_language').'?language=ruby'}}">
                    <div class="round-icon medium-round-icon" style="background: #faf2d3;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Ruby_logo.svg/2000px-Ruby_logo.svg.png">
                    </div>
                    <b>Ruby</b>
                </a>
            </li>
            <li>
                <a href="{{route('search_opening_with_language').'?language=java'}}">
                    <div class="round-icon medium-round-icon" style="background: #f9f9f9;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/%D0%9B%D0%BE%D0%B3%D0%BE_%D0%B6%D0%B0%D0%B2%D0%B0.png">
                    </div>
                    <b>Java</b>
                </a>
            </li>
            <li>
                <a href="{{route('search_opening_with_language').'?language='.urlencode('c++')}}">
                    <div class="round-icon medium-round-icon" style="background: #f9f9f9;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Cpp-Francophonie.svg/2000px-Cpp-Francophonie.svg.png">
                    </div>
                    <b>C++</b>
                </a>
            </li>
            <li>
                <a href="{{route('search_opening_with_language').'?language=python'}}">
                    <div class="round-icon medium-round-icon" style="background: #f9f9f9;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Python.svg/2000px-Python.svg.png">
                    </div>
                    <b>Python</b>
                </a>
            </li>
        </ul>
        <ul class="ud-list" style="margin-top:-50px;">
            
            <li>
                <a href="{{route('search_opening_with_language').'?language=swift'}}">
                    <div class="round-icon medium-round-icon" style="background: #f9f9f9;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Swift_logo.svg/2000px-Swift_logo.svg.png">
                    </div>
                    <b>Swift</b>
                </a>
            </li>
            <li>
                <a href="{{route('search_opening_with_language').'?language=go'}}">
                    <div class="round-icon medium-round-icon" style="background: #f9f9f9;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Gogophercolor.png/1024px-Gogophercolor.png">
                    </div>
                    <b>Go</b>
                </a>
            </li>
            <li>
                <a href="{{route('search_opening_with_language').'?language='.urlencode('c#')}}">
                    <div class="round-icon medium-round-icon" style="background: #f9f9f9;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/C_Sharp_wordmark.svg/1280px-C_Sharp_wordmark.svg.png">
                    </div>
                    <b>C#</b>
                </a>
            </li>
            <li>
                <a href="{{route('search_opening_with_language').'?language=javascript'}}">
                    <div class="round-icon medium-round-icon" style="background: #00dbdb;">
                        <img src="https://cdn.pixabay.com/photo/2017/03/30/17/41/javascript-2189147_960_720.png">
                    </div>
                    <b>Javascript</b>
                </a>
            </li>
            <li>
                <a href="{{route('search_opening_with_language').'?language=r'}}">
                    <div class="round-icon medium-round-icon" style="background: #83bada;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/R_logo.svg/2000px-R_logo.svg.png">
                    </div>
                    <b>R</b>
                </a>
            </li>
        </ul>
        <center>
            <a href="{{ url('openings') }}" class="oblong-bttn violet-bttn"> See all Jobs </a>
        </center>
        <div class="row" style="display: none;">
            <div class="col-md-6">
                <div class="simple-tile">
                    <div class="tile-icon round-icon medium-round-icon">
                        <img src="https://cdn.worldvectorlogo.com/logos/php-1.svg">
                    </div>
                    <div class="text-content">
                        <b><a href="#">Php</a></b>
                        <div>
                            PHP is a server scripting language, and a powerful tool for making dynamic and interactive Web pages.
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6">
                <div class="simple-tile">
                    <div class="tile-icon round-icon medium-round-icon">
                        <img src="{{ asset('img/python.png') }}">
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6">
                <div class="simple-tile">
                    <div class="tile-icon round-icon medium-round-icon">
                        <img src="{{ asset('img/nodejs.png') }}">
                    </div>
                    <div class="text-content">
                        <b><a href="#">NodeJS</a></b>
                        <div>
                            Node.js is an open-source, cross-platform JavaScript run-time environment for executing JavaScript code server-side.
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6">
                <div class="simple-tile">
                    <div class="tile-icon round-icon medium-round-icon">
                        <img src="{{ asset('img/ruby.png') }}">
                    </div>
                    <div class="text-content">
                        <b><a href="#">RUBY</a></b>
                        <div>
                            A ruby is a pink to blood-red colored gemstone, a variety of the mineral corundum (aluminium oxide). 
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>

        <h3 id="or-search">
            <a href="{{ url('companies') }}">
                Companies
            </a>
        </h3>
        <hr>
        <div class="row">
            <style type="text/css"></style>
            @if (count($companies) > 0)
                @foreach($companies as $company)
                    <div class="col-sm-6 col-md-4">
                        <div class="media well company-tile">
                          <div>
                            <h3 class="mt-0 mb-1" style="padding-right: 125px;">
                                <a class="ellipsis" style="display: block;" href="{{ url('companies', $company['id']) }}">
                                    {{ $company['company_name'] }}
                                    &nbsp;
                                    <br>
                                </a>
                            </h3>
                            <ul class="feature-info-list company-feature">
                              <li>
                                <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                                <div class="ellipsis">
                                    {{ $company['city'] }},
                                    {{ $company['country'] }} 
                                    &nbsp;
                                </div>
                              </li>
                              <li>
                                <i class="fa fa-users fa-lg" aria-hidden="true"></i>
                                <div class="ellipsis">
                                  {{ $company['number_of_employee'] }} <b>Employees</b>
                                  &nbsp;
                                </div>
                              </li>
                              <li>
                                <i class="fa fa-laptop fa-lg" aria-hidden="true"></i>
                                <div class="ellipsis">
                                  {{ $company['url'] }}
                                  &nbsp;
                                </div>
                              </li>
                              <li>
                                <i class="fa fa-language fa-lg" aria-hidden="true"></i>
                                <div class="ellipsis">
                                  {{ $company['bill_country'] }}
                                  &nbsp;
                                </div>
                              </li>
                              <li>
                                <i class="fa fa-file-o fa-lg" aria-hidden="true"></i>
                                <div class="ellipsis">
                                  <a href="{{ url('companies', $company['id']) }}">
                                    {{ $company->openings->count() }} Current hiring
                                    &nbsp;
                                  </a>
                                </div>
                              </li>
                            </ul>
                          <hr style="margin-top: 7px; margin-bottom: 7px;">
                          <div id="company-bookmark">
                            <div class="d-flex align-self-end ml-3" style="text-align: right;">
    <!--                                             <a href="">
                                  <i class="fa fa-star-o fa-lg" aria-hidden="true"></i>
                                  Follow
                                </a> -->
                              @include('companies.follow_company.follow_button', ['company' => $company])                                            
                            </div>
                          </div>

                          </div> <!-- media body -->
                          <img style="position:absolute; right:30px; top:15px;" class="d-flex ml-3" src="{{ $company->company_logo }}" alt="{{ $company->company_name}}" alt="{{ $company->company_name}}" height="135px;" width="135px;" />
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

        <div class="row" style="display: none;">
	    	<div class="col-md-12">
                <div class="row">
                    <div class="col-xs-5">
                        <hr class="hr">
                    </div>
                    <div class="col-xs-2">
                        <h4 class="text-center">
                        <i class="fa fa-map-o fa-lg" aria-hidden="true"></i>
                        Location
                        </h4>
                    </div>
                    <div class="col-xs-5">
                        <hr class="hr">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h3>Domestic</h3>

                        <!--LUZON-->
                            <div class="row">
                                <div class="col-xs-2 archipelago">
                                    <h4>Luzon</h4>
                                </div>
                                <div class="col-xs-10">
                                    <hr />
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Caloocan</button>
                <br />
                <button type="button" class="btn btn-primary">Mandaluyong</button>
                <br />
                <button type="button" class="btn btn-primary">Navotas</button>
                <br />
                <button type="button" class="btn btn-primary">Quezon City</button>
                <br />
                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Las Pinas</button>
                <br />
                <button type="button" class="btn btn-primary">Manila</button>
                <br />
                <button type="button" class="btn btn-primary">Paranaque</button>
                <br />
                <button type="button" class="btn btn-primary">San Juan</button>

                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Makati</button>
                <br />
                <button type="button" class="btn btn-primary">Marikina</button>
                <br />
                <button type="button" class="btn btn-primary">Pasay</button>
                <br />
                <button type="button" class="btn btn-primary">Taguid</button>

                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Malabon</button>
                <br />
                <button type="button" class="btn btn-primary">Muntinlupa</button>
                <br />
                <button type="button" class="btn btn-primary">Pasig</button>
                <br />
                <button type="button" class="btn btn-primary">Valenzuela</button>

                                </div>
                            </div>

                        <!--END LUZON-->

                        <!--VISAYAS-->
                            <div class="row">
                                <div class="col-xs-2 archipelago">
                                    <h4>Visayas</h4>
                                </div>
                                <div class="col-xs-10">
                                    <hr />
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Cebu City</button>
                <br />
                <button type="button" class="btn btn-primary">Panglao</button>
                <br />
                <button type="button" class="btn btn-primary">Bacolod City</button>
                <br />
                <button type="button" class="btn btn-primary">Dumaguete City</button>
                <br />
                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Danao City</button>
                <br />
                <button type="button" class="btn btn-primary">Lapu-Lapu City</button>
                <br />
                <button type="button" class="btn btn-primary">Ormoc city</button>
                <br />
                <button type="button" class="btn btn-primary">Iloilo City</button>

                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Kalibo</button>
                <br />
                <button type="button" class="btn btn-primary">Naga</button>
                <br />
                <button type="button" class="btn btn-primary">Tacloban</button>
                <br />
                <button type="button" class="btn btn-primary">Boljoon</button>

                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Catbalogan</button>
                <br />
                <button type="button" class="btn btn-primary">Consolacion</button>
                <br />
                <button type="button" class="btn btn-primary">Siquijor</button>
                <br />
                <button type="button" class="btn btn-primary">Oslob</button>

                                </div>
                            </div>

                        <!--END VISAYAS-->

                        <!--Mindanao-->
                            <div class="row">
                                <div class="col-xs-2 archipelago">
                                    <h4>Mindanao</h4>
                                </div>
                                <div class="col-xs-10">
                                    <hr />
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Butuan City</button>
                <br />
                <button type="button" class="btn btn-primary">Davao City</button>
                <br />
                <button type="button" class="btn btn-primary">Kidapawan City</button>
                <br />
                <button type="button" class="btn btn-primary">Cotabato City</button>
                <br />
                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Ozamiz City</button>
                <br />
                <button type="button" class="btn btn-primary">Surigao City</button>
                <br />
                <button type="button" class="btn btn-primary">Cagayan de Oro</button>
                <br />
                <button type="button" class="btn btn-primary">Panabo City</button>

                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Dipolog City</button>
                <br />
                <button type="button" class="btn btn-primary">Koronadal</button>
                <br />
                <button type="button" class="btn btn-primary">Samal City</button>
                <br />
                <button type="button" class="btn btn-primary">Zamboanga City</button>

                                </div>
                                <div class="col-xs-3">
                <button type="button" class="btn btn-primary">Illigan City</button>
                <br />
                <button type="button" class="btn btn-primary">Cotabato City</button>
                <br />
                <button type="button" class="btn btn-primary">Malaybalay City</button>
                <br />
                <button type="button" class="btn btn-primary">Pagadian City</button>

                                </div>

                            </div>

                        <!--END Mindanao-->


                    </div>

                    <div class="col-md-6">
                        <h3>International</h3>
                        <hr />
                <button type="button" class="btn btn-primary">Canada</button>
                <button type="button" class="btn btn-primary">Japan</button>
                <button type="button" class="btn btn-primary">North America</button>
                <button type="button" class="btn btn-primary">South America</button>
                <button type="button" class="btn btn-primary">Singapore</button>
                <button type="button" class="btn btn-primary">Malaysia</button>
                <button type="button" class="btn btn-primary">China</button>
                <button type="button" class="btn btn-primary">Italy</button>
                <button type="button" class="btn btn-primary">France</button>
                <button type="button" class="btn btn-primary">Australia</button>
                <button type="button" class="btn btn-primary">Netherlands</button>
                <button type="button" class="btn btn-primary">England</button>
                <button type="button" class="btn btn-primary">United Kingdom</button>
                    </div>

                </div>


        	</div>

        </div>


        <div class="row" style="margin-top:100px;">
            <div class="col-md-4">
                @include('inc.philippine-map')
            </div>
            <div class="col-md-8">
                <h3 style="margin-top:5px;">Domestic (Philippines)</h3>
                <hr>
                Maxime harum dolor doloremque ipsam a distinctio.
                Maxime harum dolor doloremque ipsam a distinctio.
                Maxime harum dolor doloremque ipsam a distinctio.
                Maxime harum dolor doloremque ipsam a distinctio.
                Maxime harum dolor doloremque ipsam a distinctio.
            </div>
        </div>

        <div class="row" style="margin-top:100px;">
            <div class="col-md-12">
                <h3>International (Worldwide)</h3>
                @include('inc.world-map')
            </div>
            <div class="col-md-8">
                <hr>
                Maxime harum dolor doloremque ipsam a distinctio.
                Maxime harum dolor doloremque ipsam a distinctio.
                Maxime harum dolor doloremque ipsam a distinctio.
                Maxime harum dolor doloremque ipsam a distinctio.
                Maxime harum dolor doloremque ipsam a distinctio.
            </div>
        </div>
    </div>

<script type="text/javascript">
 
    $(function(){
        
        $(".input-group-btn .dropdown-menu li a").click(function(){

            var selText = $(this).html();
        
           //working version - for multiple buttons //
           $(this).parents('.input-group-btn').find('.btn-search').html(selText);

       });

    });

</script>
@endsection
