<?php

namespace App\Http\Controllers;

use App\Resume;
use App\Resume_skill;
use App\User;
use App\Libs\Common;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ResumesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->middleware('onlyapplicant', ['except' => ['index', 'show']]);
        $this->middleware('navbar');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = \Auth::user();
        $month_array = Common::return_month_array();
        return view('resumes.create', compact('user', 'month_array'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'f_name' => 'required',
            'm_name' => 'required',
            'l_name' => 'required',
            'email' => 'required',
        ]);

        // dd($request);
        $input = $request->except('skills', '_token');
        // dd($input);
        // Resume::create($input);
        $resume = new Resume;
        // dd($resume);
        $resume->fill($input)->save();
        // $resume->email = $request->input('email');
        // $resume->user_id = \Auth::id();
        // $resume->civil_status = $request->input('civil_status');
        // $resume->is_active = 1;
        // $resume->is_master = 1;
        // $resume->save();
        // $saved_resume_id = Resume::where('user_id', \Auth::id())->where('is_active', 1)->where('is_master', 1)->get()->first()->id;
        // $resume = Resume::findOrFail($saved_resume_id);
        if ($request->has('skills')) {
            $resume_skill_ids = $request->input('skills');
            foreach($resume_skill_ids as $resume_skill_id) {
                $resume->has_skill()->attach($resume_skill_id);
            }
        }

        return redirect('resumes/show')->with('success', 'Registerd you resume');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $user = \Auth::user();
        $resume = Resume::where('user_id', \Auth::id())->where('is_active', 1)->where('is_master', 1)->get()->first();
        $skill_languages = Common::resume_skill_get($resume);

        // dd($resume->birth_date);
        $age = Common::cal_age($resume->birth_date);
        $birth_date = Common::month_converted_date($resume->birth_date);

        return view('resumes/show', compact('resume', 'user', 'skill_languages', 'age', 'birth_date'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($resume_id)
    {
        $resume = Resume::findOrFail($resume_id);
        $languages_ids = $resume->has_skill()->get()->lists('id')->toArray();
        return view('resumes.edit', compact('resume', 'languages_ids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $resume_id)
    {

        $this->validate($request, [
            'f_name' => 'required',
            'm_name' => 'required',
            'l_name' => 'required',
            'email' => 'required',
        ]);

        $input = $request->except('skills', '_token');
        $resume = Resume::findOrFail($resume_id);
        $resume->fill($input)->save();

        $resume->has_skill()->detach();
        $resume_skill_ids = $request->input('skills');
        foreach($resume_skill_ids as $resume_skill_id) {
            $resume->has_skill()->attach($resume_skill_id);
        }

        return redirect('resumes/show')->with('success', 'Updated your resume');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
