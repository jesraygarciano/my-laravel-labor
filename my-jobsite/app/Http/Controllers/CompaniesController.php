<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libs\Common;
use App\Company;
use App\Opening;
use Auth;

class CompaniesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->middleware('onlyhiring', ['except' => ['index', 'show','follow_companies_index','unfollow_companies_index','follow_company_lists']]);
        $this->middleware('navbar');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::latest('created_at')->where('is_active', "1")->paginate(10);
        $country_array = Common::return_country_array();

        // dd($openings->toArray());

        return view('companies.index', compact('companies','country_array'));
        // return view('companies.index', compact('companies','opening_count'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) {  //
        $rules = [    // ②
            'company_name' => 'required',
            'email' => 'required|unique:companies',
            'url' => 'required',
            // 'company_logo' => 'required|image|mimes:jpeg,png,jpg|max:4000',
            'tel' => 'required',
        ];
        // $request->user_id = Auth::user()->id;

        // Handle file upload
        if($request->hasFile('company_logo')){
            //  Get filename with the extension
            $filenameWithExt = $request->file('company_logo')->getClientOriginalName();
            //  Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just text
            $extension = $request->file('company_logo')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('company_logo')->move(public_path(). '/storage' , $fileNameToStore);
        }

        $this->validate($request, $rules);  // ③

        // Company::create($request->all());
        $request->user()->companies()->create([
            'company_name' => $request->company_name,
            'email' => $request->email,
            'is_active' => '1',
            'url' => $request->url,
            'company_logo' => $fileNameToStore,            
            'tel' => $request->tel,
        ]);

        \Session::flash('flash_message', 'created company information');

        // return redirect('companies');
        return redirect('/');
    }


// FOLLOW COMPANIES METHOD
    public function follow_companies_index($company_id)
    {

        Auth::user()->follow($company_id);
        return redirect()->back();

    }

    public function unfollow_companies_index($company_id)
    {

        Auth::user()->unfollow($company_id);

        return redirect()->back();

    }

    public function follow_company_lists(){

        $follows = Auth::user()->followings;
        return view('companies.followed_list')->with('follows', $follows);
    }

// FOLLOW COMPANIES METHOD




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $company = Company::findOrFail($id);
        $openings = Opening::where('company_id', $company->id)->get();

        //getting ids of companies that auth user created.
        $companies_ids = Common::company_ids_that_user_have();

        // dd($company);
        // dd($openings);
        // dd($companies_ids);
        return view('companies.show', compact('company', 'openings', 'companies_ids'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);

        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::findOrFail($id);

        $company->update($request->all());

        \Session::flash('flash_message', 'edited company information' );

        // return redirect(url('companies', [$company->id]));
        return redirect()->route('companies.show', [$company->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::findOrFail($id);

        $company->delete();

        \Session::flash('flash_message', 'deleted company information');

        return redirect('companies');
    }
}
