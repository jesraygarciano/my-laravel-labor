<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Opening;
use App\Company;
use App\Resume;
use App\Libs\Common;
use Auth;
use App\User;

// use Common;


class OpeningsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'searched_index_general', 'searched_index_advance', 'show']]);
        $this->middleware('onlyhiring', ['except' => ['index', 'search_opening_with_language', 'searched_index_general', 'searched_index_advance', 'show', 'bookmark_openings_index','bookmark_lists', 'unbookmark_openings_index']]);
        $this->middleware('navbar');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//     public function index()
//     {
//         // dd(Auth::user());
//         // $openings = Opening::latest('created_at')->where('is_active', 1)->get();
// /*
//         $companies = Company::find(1);
//         // $companies = Company::where('id', $openings->company_id)->get();
//
//         $openings = Opening::latest('created_at')->where('company_id', $companies->id)->where('is_active', 1)->paginate(6);
//         // dd($openings);
// */
//         $openings = Opening::latest('created_at')->where('is_active', 1)->paginate(6);
//
//         return view('openings.index', compact('openings'));
//         // return view('openings.index', compact('openings','companies'));
//     }

    public function searched_index_general(Request $request)
    {
        return view('openings.index', compact('openings'));

    }

    public function search_opening_with_language(Request $request){
        $openings = collect();
        $language = ucfirst($request->language);
        $openings_skills = \App\Opening_skill::where('language',$request->language)->get();

        foreach ($openings_skills as $skill) {
            $openings = $openings->merge($skill->openings);
        }

        return view('openings.language-search',compact('openings','language'));
    }

    public function index(Request $request)
    {

        $company_name = $request->input('company_name');

        $query = Company::query();

        if(!empty($company_name)){
            $query->where('company_name','like','%'.$company_name.'%');//why doesn't this have variable?
            $company_ids = $query->lists('id')->toArray();
            $openings = Opening::latest('created_at')->where('is_active', 1)->wherein('company_id', $company_ids)->paginate(6);
        } else{
            $openings = Opening::latest('created_at')->where('is_active', 1)->paginate(6);
            $company_ids = array();
            foreach ($openings as $opening) {
                $company_ids[] = $opening->company()->first()->id;
            }
            $company_ids = array_unique($company_ids);
            $company_ids = array_values($company_ids);

        }
        $companies = Company::wherein('id', $company_ids)->get();

        $bookmark_counts = array() ;
        foreach ($openings as $opening) {
            $bookmark_counts[$opening->id] = $opening->users_that_bookmarked()->count();
        }

        // dd($bookmark_counts);

        return view('openings.index')->with('openings', $openings)->with('company_name', $company_name)->with('companies', $companies)->with('bookmark_counts', $bookmark_counts);

    }

    public function bookmark_openings_index($opening_id)
    {

        Auth::user()->bookmark($opening_id);
        return redirect()->back();

    }

    public function unbookmark_openings_index($opening_id)
    {

        Auth::user()->unbookmark($opening_id);

        return redirect()->back();

    }

    public function bookmark_lists(){

        $bookmarks = Auth::user()->bookmarkings;
        return view('openings.bookmarked_list')->with('bookmarks', $bookmarks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($company_id)
    {
        $country_array = Common::return_country_array();
        return view('openings.create', compact('country_array','company_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->has('skills')) {
            $request->skills = "";
        }

        $this->validate($request, [
            'title' => 'required',
            'requirements' => 'required',
            // 'picture' => 'required',
            'skills' => 'required',

        ]);

        // Handle file upload
        if($request->hasFile('picture')){
            //  Get filename with the extension
            $filenameWithExt = $request->file('picture')->getClientOriginalName();
            //  Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just text
            $extension = $request->file('picture')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('picture')->move(public_path(). '/storage' , $fileNameToStore);
        }

        // Create Opening
        $opening = new Opening;
        $opening->title = $request->input('title');
        $opening->company_id = $request->input('company_id');
        $opening->requirements = $request->input('requirements');
        if($request->hasFile('picture')){
            $opening->picture = $fileNameToStore;
        }
        $opening->save();
        $opening->register_skill($request->input('skills'));

        return redirect('hiring_portal');
        // return redirect()->action('HiringPortalController@show', [$request->input('company_id')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $opening = Opening::findOrFail($id);
        $company = Company::where('id', $opening->company_id)->get()->first();
        $resume = array();
        if(Auth::check()){
            $resume = Resume::where('user_id', Auth::user()->id)->where('is_master', 1)->where('is_active', 1)->get()->first();
        }
        // dd($resume);

        return view('openings.show', compact('opening','company', 'resume'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
