<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Intern;
use App\Resume;
use App\Company;
use App\Opening;
use App\User;
use App\Scout;
use App\Opening_skill;
use App\Resume_skill;
use App\Application;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

            $this->call('InternsTableSeeder');
            $this->call('UsersTableSeeder');
            $this->call('CompaniesTableSeeder');
            $this->call('OpeningsTableSeeder');
            $this->call('ResumesTableSeeder');
            $this->call('ApplicationsTableSeeder');
            $this->call('ScoutsTableSeeder');
            $this->call('OpeningskillsTableSeeder');
            $this->call('ResumeskillsTableSeeder');
            $this->call('OpeningsFillCountryProvinceColumnSeeder');


        Model::reguard();
    }
}

class InternsTableSeeder extends Seeder  // ③
{
    public function run()
    {
        // DB::table('interns')->delete();  // ④

        $faker = Faker::create('en_US');  // ⑤

        for ($i = 0; $i < 40; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            Intern::create([
                'email' => $faker->email(),
                'password' => $faker->password(),
                'nickname' => $faker->name(),
                'picture' => $faker->url(),
                'is_active' => $faker->boolean(),
                'created_at' => $datetime,
                'updated_at' => $datetime
                // 'updated_at' => Carbon::today()
            ]);
        }
    }
}

class ResumesTableSeeder extends Seeder  // ③
{
    public function run()
    {
        // DB::table('resumes')->delete();  // ④

        $graduate_flag = ['0', '1'];
        $gender = ['f', 'm'];

        $is_active = ['0', '1'];
        $is_master = ['0', '1'];

        // 0 = deleted, 1 = alive

        // $user_id = ['1', '2', '3', '4', '5'];
        $user_id = array();
        for ($i = 1; $i < 21; $i++) {
            $user_id[] = $i;
        }

        $faker = Faker::create('en_US');  // ⑤

        for ($i = 0; $i < 40; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            Resume::create([
                'email' => $faker->email(),
                'objective' => $faker->text($maxNbChars = 500),
                'nationality' => $faker->country(),
                'civil_status' => $faker->country,
                'gender' => $faker->randomElement($gender),
                'experience' => $faker->text($maxNbChars = 500),
                'postal' => $faker->postcode(),
                'address1' => $faker->streetAddress(),
                'address2' => $faker->streetName(),
                'city' => $faker->city(),
                'country' => $faker->country(),
                'phone_number' => $faker->phoneNumber(),
                'photo' => $faker->lexify('photo_???????????.png'),
                'created_at' => $datetime,
                'updated_at' => $datetime,
                'university' => $faker->city(),
                'graduate_flag' => $faker->randomElement($graduate_flag),
                'program_of_study' => $faker->text($maxNbChars = 500),
                'field_of_study' => $faker->text($maxNbChars = 500),
                'is_active' => $faker->randomElement($is_active),
                'is_master' => $faker->randomElement($is_master),
                'user_id' => $faker->randomElement($user_id)
            ]);
        }
    }
}

class UsersTableSeeder extends Seeder  // ③
{
    public function run()
    {
        // DB::table('users')->delete();  // ④

        $gender = ['f', 'm'];
        $role = ['0', '1', '2'];
        // 0 = student, 1 = hiring , 2 = website manager
        $is_active = ['0', '1'];
        // 0 = deleted, 1 = alive
        $graduate_flag = ['0', '1'];
        // 0 = under graduate 1 = graduated

        $faker = Faker::create('en_US');  // ⑤
        for ($i = 0; $i < 20; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            User::create([
                'email' => $faker->email(),
                'password' => $faker->password(),
                'role' => $faker->randomElement($role),
                'university' => $faker->firstName,
                'graduate_flag' => $faker->randomElement($graduate_flag),
                'program_of_study' => $faker->firstName,
                'field_of_study' => $faker->firstName,
                'birth_date' => $faker->date(),
                'gender' => $faker->randomElement($gender),
                'f_name' => $faker->firstName,
                'l_name' => $faker->lastName,
                'm_name' => $faker->firstName,
                'postal' => $faker->postcode(),
                'address1' => $faker->streetAddress(),
                'address2' => $faker->streetName(),
                'city' => $faker->city(),
                'country' => $faker->country(),
                'phone_number' => $faker->phoneNumber(),
                'photo' => $faker->lexify('photo_???????????.png'),
                'objective' => $faker->text($maxNbChars = 500),
                'is_active' => $faker->randomElement($is_active),
                'created_at' => $datetime,
                'updated_at' => $datetime
            ]);
        }
    }
}

class CompaniesTableSeeder extends Seeder  // ③
{
    public function run()
    {
        // DB::table('companies')->delete();  // ④

        $is_active = ['0', '1'];
        // 0 = deleted, 1 = alive

        // $user_id = ['1', '2', '3', '4', '5'];
        $user_id = array();
        for ($i = 1; $i < 21; $i++) {
            $user_id[] = $i;
        }

        // var_export($user_id);

        $faker = Faker::create('en_US');  // ⑤
        for ($i = 0; $i < 20; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            $company_name = $faker->company();
            // var_export($company_name);
            Company::create([
                'company_name' => $company_name,
                'email' => $faker->email(),
                'password' => $faker->password(),
                'in_charge' => $faker->name(),
                'ceo_name' => $faker->name(),
                'postal' => $faker->postcode(),
                'address1' => $faker->streetAddress(),
                'address2' => $faker->streetName(),
                'city' => $faker->city(),
                'country' => $faker->country(),
                'url' => $faker->url(),
                'tel' => $faker->phoneNumber(),
                'number_of_employee' => $faker->numberBetween($min = 10, $max = 9000),
                'established_at' => $faker->date(),
                'facebook_url' => $faker->lexify('??????'),
                'twitter_url' => $faker->lexify('???????????'),
                'company_logo' => $faker->lexify('company???????????.jpg'),
                'background_photo' => $faker->lexify('back???????????.jpg'),
                'company_introduction' => $faker->text($maxNbChars = 300),
                'what' => $faker->text($maxNbChars = 500),
                'what_photo1' => $faker->lexify('what_photo1???????????.png'),
                'what_photo1_explanation' => $faker->text($maxNbChars = 400),
                'what_photo2' => $faker->lexify('what_photo2???????????.png'),
                'what_photo2_explanation' => $faker->text($maxNbChars = 400),
                'bill_company_name' => $company_name,
                'bill_postal' => $faker->postcode(),
                'bill_address1' => $faker->streetAddress(),
                'bill_address2' => $faker->streetName(),
                'bill_city' => $faker->city(),
                'bill_country' => $faker->country(),
                'is_active' => $faker->randomElement($is_active),
                // 'user_id' => $faker->'1',
                'user_id' => $faker->randomElement($user_id),
                'created_at' => $datetime,
                'updated_at' => $datetime
            ]);
        }
    }
}

class OpeningsTableSeeder extends Seeder  // ③
{
    public function run()
    {
        // DB::table('openings')->delete();  // ④

        $is_active = ['0', '1', '1', '1', '1', '1'];
//      // 0 = deleted, 1 = alive


        // $company_id = ['5', '6', '7', '8', '9'];
        $company_id = array();
        for ($i = 1; $i < 21; $i++) {
            $company_id[] = $i;
        }

        $faker = Faker::create('en_US');  // ⑤

        for ($i = 1; $i < 40; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            Opening::create([
                'title' => $faker->lexify('title_???????????'),
                'address1' => $faker->address(),
                'address2' => $faker->address(),
                'picture' => $faker->url(),
                'icon' => $faker->url(),
                'details' => $faker->sentence(),
                'requirements' => $faker->sentence(),
                'term' => $faker->sentence(),
                'other' => $faker->sentence(),
                'is_active' => $faker->randomElement($is_active),
                'company_id' => $faker->randomElement($company_id),
                'created_at' => $datetime,
                'updated_at' => $datetime
            ]);
        }
    }
}

class ApplicationsTableSeeder extends Seeder  // ③
{
    public function run()
    {
        DB::table('applications')->delete();  // ④

        $is_active = ['0', '1', '1', '1', '1', '1'];
//      // 0 = deleted, 1 = alive


        // $user_id = ['5', '6', '7', '8', '9'];
        $user_id = array();
        for ($i = 1; $i < 21; $i++) {
            $user_id[] = $i;
        }
        // $company_id = ['5', '6', '7', '8', '9'];

        $opening_id = array();
        for ($i = 1; $i < 21; $i++) {
            $opening_id[] = $i;
        }

        $resume_id = array();
        for ($i = 1; $i < 21; $i++) {
            $resume_id[] = $i;
        }        

        $faker = Faker::create('en_US');  // ⑤

        for ($i = 1; $i < 40; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            Application::create([

                'is_active' => $faker->randomElement($is_active),
                'description' => $faker->sentence(),
                'user_id' => $faker->randomElement($user_id),
                'opening_id' => $faker->randomElement($opening_id),
                'resume_id' => $faker->randomElement($resume_id),                
                'created_at' => $datetime,
                'updated_at' => $datetime
            ]);
        }
    }
}

class ScoutsTableSeeder extends Seeder  // ③
{
    public function run()
    {

        $is_active = ['0', '1'];

        // $user_id = ['1', '2', '3', '4', '5'];
        $user_id = array();
        for ($i = 1; $i < 21; $i++) {
            $user_id[] = $i;
        }

        // $company_id = ['5', '6', '7', '8', '9'];
        $company_id = array();
        for ($i = 1; $i < 21; $i++) {
            $company_id[] = $i;
        }        

        $faker = Faker::create('en_US');  // ⑤

        for ($i = 0; $i < 40; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            Scout::create([
                'description' => $faker->sentence(),                
                'is_active' => $faker->randomElement($is_active),
                'user_id' => $faker->randomElement($user_id),
                'company_id' => $faker->randomElement($company_id),                
                'created_at' => $datetime,
                'updated_at' => $datetime
                // 'updated_at' => Carbon::today()
            ]);
        }
    }
}

class OpeningskillsTableSeeder extends Seeder  // ③
{
    public function run()
    {

        $faker = Faker::create('en_US');  // ⑤

        for ($i = 0; $i < 40; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            Opening_skill::create([
                'language' => $faker->country(),
                'category' => $faker->sentence(),                
                'created_at' => $datetime,
                'updated_at' => $datetime
            ]);
        }
    }
}

class ResumeskillsTableSeeder extends Seeder  // ③
{
    public function run()
    {

   $faker = Faker::create('en_US');  // ⑤

        for ($i = 0; $i < 40; $i++) {  // ⑥
            $datetime = $faker->dateTimeThisYear();
            Resume_skill::create([
                'language' => $faker->country(),
                'category' => $faker->sentence(),
                'created_at' => $datetime,
                'updated_at' => $datetime
            ]);
        }
    }
}

class OpeningsFillCountryProvinceColumnSeeder extends Seeder
{
    public function run()
    {
        foreach (Opening::all() as $opening) 
        {
            // default is Philippines - PHL
            $country_code ="PHL";

            // fetch provinces
            $provinces = \DB::table('provinces')->where('country_code','PHL')->get();

            $ran = rand(0,count($provinces) - 1);

            $opening->province_code = $provinces[$ran]->iso_code;
            $opening->country_code = $country_code;
            $opening->save();

        }
    }
}


