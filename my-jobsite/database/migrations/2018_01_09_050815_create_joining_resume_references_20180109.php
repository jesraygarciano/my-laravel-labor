<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJoiningResumeReferences20180109 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joining_resume_references', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('resume_id')->unsigned()->index();
            $table->foreign('resume_id')->references('id')->on('resumes');

            $table->integer('character_reference_id')->unsigned()->index();
            $table->foreign('character_reference_id')->references('id')->on('character_references');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('joining_resume_references');
    }
}
