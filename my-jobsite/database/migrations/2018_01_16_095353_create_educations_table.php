<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('university');
            $table->string('program_of_study');
            $table->string('field_of_study');
            $table->string('country');
            $table->string('city');
            $table->integer('from_year');
            $table->integer('from_month');
            $table->integer('to_year');
            $table->integer('to_month');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('educations');
    }
}
