<?php
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;

$botman = resolve('botman');

$botman->hears('pikopoko', function ($bot) {
    $bot->reply('Shohei');
});

$botman->hears('How about jesray?', function ($bot) {

       // Create attachment
       $attachment = new Image('https://s3.amazonaws.com/hiphopdx-production/2015/10/drake-hotline-bling-jacket-moncler.png');

       // Build message object
       $message = OutgoingMessage::create('Not Pikopoko!')
                   ->withAttachment($attachment);

       // Reply message object
       $bot->reply($message);

});

$botman->hears('Is Shohei a pikopoko?', function ($bot) {

       // Create attachment
       $attachment = new Image('https://i.imgur.com/THvJBNQ.jpg');

       // Build message object
       $message = OutgoingMessage::create('Pikopoko!')
                   ->withAttachment($attachment);
       // Reply message object
       $bot->reply($message);

});
$botman->hears('/random', 'App\Http\Controllers\AllBreedsController@random');

$botman->hears('/b {breed}', 'App\Http\Controllers\AllBreedsController@byBreed');

$botman->hears('/s {breed}:{subBreed}', 'App\Http\Controllers\SubBreedController@random');

$botman->hears('Start conversation', 'App\Http\Controllers\ConversationController@index');

$botman->fallback('App\Http\Controllers\FallbackController@index');

// D toturials here
// https://scotch.io/tutorials/build-a-telegram-bot-with-laravel-and-botman#toc-introduction
